<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/master', function(){
//     return view('layouts.master');
// });

Route::get('/home', function(){
    return view ('home');
});


//Create Data
//route untuk mengarah ke form tambah kategori
Route::get('/category/create', [CategoryController::class, 'create']);
//route untuk menyimpan data inputan ke database table kategori
Route::post('/category', [CategoryController::class, 'store']);

//Read Data
//route untuk menampilkan semua data yang ada di table kategori database
Route::get('/category', [CategoryController::class, 'index']);
//route untuk menampilkan detail data kategori berdasarkan id
Route::get('/category/{id}', [CategoryController::class, 'show']);

//Update Data
//route untuk mengarah ke form edit category dengan membawa data berdasarkan id
Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);
//route untuk mengupdate data pemain film berdasarkan id
Route::put('/category/{id}', [CategoryController::class, 'update']);

//Delete Data
//route untuk menghapus data category berdasarkan id
Route::delete('/category/{id}', [CategoryController::class, 'destroy']);

Route::resource('product', ProductController::class);

Route::resource('keranjang', KeranjangController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
