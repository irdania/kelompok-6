@extends('layouts.master')
@section('title')
    Halaman Tambah Category
@endsection

@section('content')
    <form method="POST" action="/category">
        @csrf
        <div class="form-group">
            <label>Cetegory Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
@endsection
