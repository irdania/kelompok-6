@extends('layouts.master')
@section('title')
    Halaman Edit Data Category
@endsection

@section('content')
    <form method="POST" action="/category/{{ $category->id }}">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Cetegory Name</label>
            <input type="text" value="{{$category->name}}" name="name" class="form-control">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10">{{$category->description}}</textarea>
        </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
@endsection
