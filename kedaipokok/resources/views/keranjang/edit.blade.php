@extends('layouts.master')
@section('title')
    Halaman Edit Data Keranjang
@endsection

@section('content')
    <form method="POST" action="/keranjang/{{ $keranjang->id }}">
        @csrf
        @method('put')
        <div class="form-group">
            <label>keranjang Name</label>
            <input type="text" value="{{$keranjang->name}}" name="name" class="form-control">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10">{{$keranjang->description}}</textarea>
        </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
@endsection
