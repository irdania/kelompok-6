@extends('layouts.master')
@section('title')
    Keranjang
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h2>{{ $cart->order_id }}</h2>
            <p class="card-text">{{ $cart->product_id }}</p>
            <p class="card-text">{{ $cart->quantity }}</p>
            <a href="/product" class="btn btn-block btn-info">Back</a>
        </div>
    </div>
@endsection
