@extends('layouts.master')
@section('title')
    Keranjang Belanja
@endsection

@section('content')
    <div class="jumbotron text-center">
        <p>Terima kasih sudah belanja di toko kami</p>
        <p>Senang melayani anda</p>
        <div class="container">
            @forelse ($cart as $item)
                <div class="card-text">{{ $item->category_id }}</div>
                <div class="card-text">{{ $item->product_id }}</div>
                <div class="card-text">{{ $item->quantity }}</div>
            @empty
                <h4>Belum ada product</h4>
            @endforelse
        </div>
    </div>
    @endsection
