@extends('layouts.master')
@section('title')
    Halaman Data Product
@endsection

@section('content')
    <a href="/product/create" class="btn btn-primary btn-sm my-2">Tambah</a>

    <div class="col my-3">
        @forelse ($product as $item)
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('/images/' . $item->image) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h2>{{ $item->name }}</h2>
                    <p class="card-text">{{ $item->price }}</p>
                    <form action="/product/{{ $item->id }}" method="POST">
                        <a href="/product/{{ $item->id }}" class="btn btn-block btn-info">Detail</a>
                        <a href="/product/{{ $item->id }}/edit" class="btn btn-block btn-warning">Edit</a>
                        @csrf
                        @method('Delete')
                        <input type="submit" value="Delete" class="btn btn-block btn-danger">
                    </form>
                </div>
            </div>
    </div>
@empty
    <h4>Belum ada product</h4>
    @endforelse

    </div>
@endsection
