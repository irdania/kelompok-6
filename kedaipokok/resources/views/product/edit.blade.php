@extends('layouts.master')
@section('title')
    Halaman Edit Product
@endsection

@section('content')
    <form method="POST" action="/product/{{$product->id}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Product Name</label>
            <input type="text" name="name" value="{{ $product->name }}" class="form-control">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" value="{{ $product->price }}" class="form-control">
        </div>
        @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Category</label>
            <select name="category_id" id="" class="form-control">
                <option value="">--Choose Category--</option>
                @forelse ($category as $item)
                    @if ($item->id === $product->category_id)
                        <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endif
                @empty
                    <option value="">Belum ada Data Kategori</option>
                @endforelse
            </select>
        </div>
        @error('category_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Image</label>
            <input type="file" name="image" class="form-control">
        </div>
        @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
@endsection
