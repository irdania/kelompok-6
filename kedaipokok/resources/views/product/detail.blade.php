@extends('layouts.master')
@section('title')
    Halaman Detail Produk
@endsection

@section('content')
    <div class="card">
        <img src="{{ asset('/images/' . $product->image) }}" class="card-img-top" alt="...">
        <div class="card-body">
            <h2>{{ $product->name }}</h2>
            <p class="card-text">{{ $product->price }}</p>
            <a href="/product" class="btn btn-block btn-info">Back</a>
        </div>
    </div>
@endsection
