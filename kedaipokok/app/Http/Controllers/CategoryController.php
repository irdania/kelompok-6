<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function create()
    {
        return view('category.tambah');
    }

    public function store(request $request)
    {
        //validasi data
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'required'
        ]);

        //masukan data request ke table category di db
        DB::table('category')->insert([
            'name' => $request['name'],
            'description' => $request['description']
        ]);

        //kita lempar ke halaman /category
        return redirect('/category');
    }

    public function index()
    {
        $category = DB::table('category')->get();

        return view('category.index', ['category' => $category]);
    }

    public function show($id)
    {
        $category = DB::table('category')->find($id);

        return view('category.detail', ['category' => $category]);
    }

    public function edit($id)
    {
        $category = DB::table('category')->find($id);

        return view('category.edit', ['category' => $category]);
    }

    public function update($id, Request $request)
    {
        //validasi data
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'required'
        ]);

        //update data
        DB::table('category')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request['name'],
                    'description' => $request['description']
                ]
            );

        //lempar ke url /data
        return redirect('/category');
    }

    public function destroy($id)
    {
        DB::table('category')->where('id', $id)->delete();

        return redirect('/category');
    }
}
