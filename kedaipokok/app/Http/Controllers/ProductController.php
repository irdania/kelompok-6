<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return view('product.index', ['product' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('product.tambah', ['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi data
        $request->validate([
            'name' => 'required|min:5',
            'price' => 'required',
            'category_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
        ]);

        $newNameImage = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $newNameImage);

        $product = new Product;

        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->category_id = $request['category_id'];
        $product->image = $newNameImage;

        $product->save();

        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.detail', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::all();
        return view('product.edit', ['product' => $product, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi data
        $request->validate([
            'name' => 'required|min:5',
            'price' => 'required',
            'category_id' => 'required',
            'image' => 'mimes:png,jpg,jpeg',
        ]);

        $product = Product::find($id);

        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->category_id = $request['category_id'];

        if ($request->has('image')) {
            $path = 'images/';
            File::delete($path . $product->image);

            $newNameImage = time() . '.' . $request->image->extension();

            $request->image->move(public_path('images'), $newNameImage);

            $product->image = $newNameImage;
        }

        $product->save();

        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $path = 'images/';
        File::delete($path . $product->image);

        $product->delete();

        return redirect('/product');
    }
}
