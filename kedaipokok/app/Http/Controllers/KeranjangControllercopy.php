<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KeranjangController extends Controller
{
    public function index()
    {
        $keranjang = DB::table('keranjang')->get();
 
        return view('keranjang.index', ['keranjang' => $keranjang]);
    }
    
    public function store(request $request)
    {
        $request->validate([
            'product' => 'required',
            'name' => 'required',
            'price' => 'required',
            'image' => 'required',
        ]);
        DB::table('keranjang')->insert([
            'product' => $request['keranjang'],
            'name' => $request['name'],
            'price' => $request['price'],
            'image' => $request['image'],
        ]);

        return redirect('/keranjang');
    }

    public function show($id)
    {
        $keranjang = DB::table('keranjang')->where('id' , $id)->firs();
        return view('keranjang.show', ['keranjang' => $keranjang]);
    }

    public function edit($id)
    {
        $keranjang = DB::table('keranjang')->where('id' , $id)->firs();
        return view('keranjang.edit', ['keranjang' => $keranjang]);
    }

    public function update(request $request, $id)
    {
        $request->validate([
            'product' => 'required',
            'name' => 'required',
            'price' => 'required',
            'image' => 'required',
        ]);

        DB::table('keranjang')
            ->where('id', $id)
            ->update(
                [
                    'product' => $request['keranjang'],
                    'name' => $request['name'],
                    'price' => $request['price'],
                    'image' => $request['image'],
                ]

            );

        return redirect('/keranjang');
    }

    public function destroy($id)
    {
        DB::table('keranjang')->where('id', '=', $id)->delete();
        return redirect('/keranjang');
    }
}

